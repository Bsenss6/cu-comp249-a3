/*
 * csv2json: a program to convert CSV files into JSON files.
 * Author: Dannick BUJOLD-SENSS.
 * Student ID: 40180290.
 * Course: COMP 249, Section QQ.
 * With: Dr. Kaustubha Mendhurwar.
 * Project: Assignment 3.
 */

package comp249.qq.a3;

import comp249.qq.a3.cla.ArgsParser;
import comp249.qq.a3.cla.Options;
import comp249.qq.a3.exceptions.cla.CLAException;
import comp249.qq.a3.exceptions.csv.CSVDataMissingException;
import comp249.qq.a3.exceptions.csv.CSVFileInvalidException;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

/**
 * Convert files from CSV to JSON
 *
 * <p>csv2json reads files with data written in CSV format and convert them
 * as new files written in JSON format.</p>
 *
 * <p>If the program is run with the interactive flag (-i, --interactive), it attempts to read and parse the given
 * CSV files and to write them in a new file in JSON format. It them prompts the user for the name of a json file
 * to display, and that file is printed to standard output.</p>
 */
public class CSV2JSON {

    //
    //               | |
    //               | |
    //             __| |__
    //             \     /
    //              \   /
    //               \ /
    //                V
    //
    // +-----------------------------+
    // | CHANGE THE INPUT FILES HERE |
    // +-----------------------------+
    private static final String[] DEFAULT_ARGS = {
            "--interactive",
            "--log", "res/out/log.txt",       // LOG FILE
            "--directory", "res/out",         // OUTPUT DIRECTORY
            "res/Car Maintenance Record.csv", // INPUT FILE I
            "res/Car Rental Record no Plate.csv",
            "res/Car Rental Record no DrivLic.csv",
            "res/Car Rental Record.csv"       // INPUT FILE II (or add more)
    };

    // Could specify more accurate exit statuses if useful
    private static final int EXIT_SUCCESS = 0;
    private static final int EXIT_FAILURE = 1;

    private static PrintWriter logFile;
    private static Options options;

    /**
     * Give access to log messages in all project
     *
     * <p>The given message is printed in the log file.</p>
     *
     * @param message message to be printed in log file
     */
    public static void logPrintln(String message) {
        logFile.println(message);
    }

    /**
     * Returns the directory where the output files are to be created.
     * @return The directory where the output files are to be created.
     */
    public static Path getOutputDirectory() {
        return options.getOutputDirectory();
    }

    /**
     * Returns whether the JSON output should be written in compact form.
     *
     * <p>When this flag is set to <tt>true</tt>, the indentation are removed and everything is
     * written on a single line.</p>
     * @return Whether the JSON output should be written in compact form.
     */
    public static boolean isCompactJSON() {
        return options.isCompactJSON();
    }

    public static void main(String[] args) {

        // Parse arguments
        try {
//            ArgsParser argsParser = new ArgsParser(args);
            ArgsParser argsParser = new ArgsParser(DEFAULT_ARGS);
            options = argsParser.parse();
        } catch (CLAException e) {
            System.err.println(e.getMessage());
            displayUsage();
            System.exit(EXIT_FAILURE);
        }

        if (options.isHelpLong()) {
            displayHelpLong();
            System.exit(EXIT_SUCCESS);
        }

        if (options.isHelpShort()) {
            displayHelpShort();
            System.exit(EXIT_SUCCESS);
        }

        if (options.isAbout()) {
            displayAbout();
            System.exit(EXIT_SUCCESS);
        }

        if (options.isQuiet()) {
            // Set System.out to an output stream that doesn't print anything
            System.setOut(new PrintStream(OutputStream.nullOutputStream()));
        }

        // Welcome banner
        System.out.println("-- STARTING EXECUTION OF PROGRAM --");

        // Open log file
        try {
            logFile = new PrintWriter(new BufferedWriter(new FileWriter(options.getLogFile().toFile())));
        } catch (IOException e) {
            System.err.println("Could not open log file '" + options.getLogFile() + "' for writing.");
            System.err.println("Program will terminate.");
            System.exit(EXIT_FAILURE);
        }

        // Open input files
        CsvFileConverter[] inputFiles = null;
        try {
            inputFiles = openInputFiles();
        } catch (FileNotFoundException e) {
            logFile.flush();
            logFile.close();
            System.exit(EXIT_FAILURE);
        }

        // Run the program
        processFilesForValidation(inputFiles);

        // Closing banner
        System.out.println("-- END OF EXECUTION OF PROGRAM --");
    }

    /**
     * Attempts to open all the input files for reading.
     *
     * @return An array of {@link CsvFileConverter}s representing the open files.
     * @throws FileNotFoundException when a file cannot be opened for reading.
     */
    private static CsvFileConverter[] openInputFiles() throws FileNotFoundException {
        Path[] paths = options.getFiles();
        CsvFileConverter[] result = new CsvFileConverter[paths.length];

        int i = 0;
        try {
            while (i < paths.length) {
                result[i] = CsvFileConverter.fromPath(paths[i]);
                i++;
            }
        } catch (FileNotFoundException e) {
            // Close all opened files.
            for (int j = 0; j < i; j++) {
                result[i].close();
            }
            // Inform main of failure to exit
            throw e;
        }
        return result;
    }

    /**
     * Core engine for processing the input files and creating the output ones.
     *
     * <p>This method works on already opened files to verify their validity.</p>
     * <p>If a file is valid, a JSON file is created and written.</p>
     * <p>If it is invalid, a {@link CSVFileInvalidException} is thrown and the
     * processing of the file is stopped. The error is then printed to a log file.</p>
     * <p>If the file is valid, but an invalid record is found, a {@link CSVDataMissingException} is thrown,
     * that record is ignored, and a message is printed to the user and saved to the log file.</p>
     * @param files array containing the open files for processing
     */
    private static void processFilesForValidation(CsvFileConverter[] files) {

        // Process each file for validation
        for (CsvFileConverter file : files) {
            try {
                // Check file validity
                file.parseHeader();

                // Create output file
                file.openOutputFile();

                // Parse file to JSON
                file.convertFile();

                // Close
                file.close();

            } catch (CSVFileInvalidException e) {
                System.err.println(e.getMessage());
                System.err.println("File is not converted to JSON.");
                file.close();
            } catch (IOException e) {
                logFile.println(e.getMessage());
                System.err.println(e.getMessage());
                System.err.println("Program will terminate after closing all input files.");

                // Close all and exit
                cleanAllOutputFiles(files);
                Arrays.stream(files).forEach(CsvFileConverter::close);
                logFile.flush();
                logFile.close();
                System.exit(EXIT_FAILURE);
            }
        }

        // Prompt user for file to display
        if (options.isInteractive()) {
            // List of possible files to be displayed
            printFileContent(Arrays.stream(files)
                    .filter(CsvFileConverter::isOutputCreated)
                    .toArray(CsvFileConverter[]::new) );
        }
    }

    /**
     * Delete all created output files.
     *
     * Called in case an output file could not be created.
     * @param files reference to files to be deleted
     */
    private static void cleanAllOutputFiles(CsvFileConverter[] files) {
        Path outDir = options.getOutputDirectory();

        // Remove all opened output files
        Arrays.stream(files)
                .filter(CsvFileConverter::isOutputCreated)
                .map(CsvFileConverter::getOutputFileName)
                .map(outDir::resolve)
                .forEach(file -> {
                    try {
                        Files.delete(file);
                    } catch (IOException e) { /* Ignore */ }
                });
    }

    /**
     * Prints the content of a file.
     * <p>This is called when the program is launched in interactive mode. The user is prompted for the name of a file,
     * and that file is printed to standard output using this method.</p>
     * @param files list of files that can be displayed
     */
    private static void printFileContent(CsvFileConverter[] files) {
        int remainingAttempts = 2;
        Scanner scanner = new Scanner(System.in);
        Path outDir = options.getOutputDirectory();

        while (remainingAttempts > 0) {
            System.out.println("Type the name of a file to be displayed:");
            for (CsvFileConverter file : files) {
                System.out.println(file.getOutputFileName());
            }
            System.out.print("> ");

            String input = scanner.nextLine();
            Path path = outDir.resolve(input);


            try (BufferedReader reader = new BufferedReader(new FileReader(path.toFile()))) {

                // Print all content of file to stdout
                reader.lines().forEach(System.out::println);

            } catch (FileNotFoundException e) {
                System.err.println("Wrong file name.");
                System.err.println("No such file '" + path + "'.");
                remainingAttempts--;
                continue;
            } catch (IOException e) {
                System.err.println("I/O error occurred while attempting to read file.");
                System.err.println("Program will terminate.");
                logFile.flush();
                logFile.close();
                System.exit(EXIT_FAILURE);
            }

            remainingAttempts = 0;
        }
    }

    /**
     * Displays the long help message.
     * <p>This help message shows detailed information about the usage and the various options available.</p>
     */
    private static void displayHelpLong() {
        // Can't wait for Text Blocks support in Java 15
        String helpLong = ""
                + "NAME\n"
                + "    csv2json - convert file from csv to json\n"
                + "\n"
                + "SYNOPSIS\n"
                + "    csv2json [OPTION]... FILE...\n"
                + "    csv2json [OPTION]... -d DIR FILE...\n"
                + "\n"
                + "DESCRIPTION\n"
                + "    Read data FILE(s) in CSV format to convert them into JSON files.\n"
                + "\n"
                + "    -d DIR, --directory DIR\n"
                + "        Specifies the DIR where to write the json files. Defaults to the current\n"
                + "        directory.\n"
                + "\n"
                + "    -l LOG, --log LOG\n"
                + "        Specifies the name of the file where to write the log messages. Defaults\n"
                + "        to 'log.txt' in the current directory.\n"
                + "\n"
                + "    -c, --compact-json\n"
                + "        Writes the JSON files in a compact manner. This means that no whitespace\n"
                + "        and  no line break is inserted in the file. Everything is written on the\n"
                + "        same line. This can be useful when sending the data to a stream where  a\n"
                + "        human readable format is unnecessary.\n"
                + "\n"
                + "    -i, --interactive\n"
                + "        Start csv2json in interactive mode.     The csv files to be  parsed  are\n"
                + "        converted to json format and written to files. The user is then prompted\n"
                + "        for  the  name of a file to display, and that file is printed to stdout.\n"
                + "\n"
                + "    -q, --quiet\n"
                + "        Do  not write anything to standard output. This is incompatible with the\n"
                + "        --interactive flag.   If both flags are specified, --quiet  is  ignored.\n"
                + "\n"
                + "    -a, --about\n"
                + "        Displays information about this program, then exits.\n"
                + "\n"
                + "    -h, --help\n"
                + "        Displays this help, then exits. Use -h for a shorter help message.\n"
                + "\n"
                + "AUTHOR\n"
                + "    Dannick BUJOLD-SENSS\n"
                + "    Concordia University Student ID: 40180290\n"
                + "\n"
                + "SEE ALSO\n"
                + "    CSV specification at: <https://csv-spec.org>\n"
                + "    JSON specification at: <https://www.json.org/json-en.html>";

        System.out.println(helpLong);
    }

    /**
     * Displays the short help message.
     * <p></p>This help message shows the usage and list the various options available.</p>
     */
    private static void displayHelpShort() {
        // Can't wait for Text Blocks support in Java 15
        String helpMsg = ""
                + "Usage: csv2json [OPTIONS] FILE ...\n"
                + "Read data FILE(s) in CSV format to convert them into JSON files.\n"
                + "\n"
                + "  -d, --directory <DIR>    Writes output files inside DIR.\n"
                + "  -l, --log <LOG>          Writes log messages to LOG.\n"
                + "  -c, --compact-json       Writes JSON files in a compact manner.\n"
                + "  -q, --quiet              Do not print anything to stdout.\n"
                + "  -i, --interactive        Start csv2json in interactive mode.\n"
                + "  -a  --about              Displays information about this program and exits.\n"
                + "  -h, --help               Displays this help and exits. Try --help for more details.";

        System.out.println(helpMsg);
    }

    /**
     * Displays the usage of this program.
     */
    private static void displayUsage() {
        String usage = "Usage: csv2json [OPTIONS] FILE ...";
        System.out.println(usage);
    }

    /**
     * Displays information about the program.
     */
    private static void displayAbout() {
        String aboutMsg = ""
                + "csv2json: a program to convert CSV files into JSON files.\n"
                + "Author: Dannick BUJOLD-SENSS.\n"
                + "Student ID: 40180290.\n"
                + "Course: COMP 249, Section QQ.\n"
                + "With: Dr. Kaustubha Mendhurwar.\n"
                + "Project: Assignment 3.";

        System.out.println(aboutMsg);
    }
}
