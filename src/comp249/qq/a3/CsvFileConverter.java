/*
 * csv2json: a program to convert CSV files into JSON files.
 * Author: Dannick BUJOLD-SENSS.
 * Student ID: 40180290.
 * Course: COMP 249, Section QQ.
 * With: Dr. Kaustubha Mendhurwar.
 * Project: Assignment 3.
 */

package comp249.qq.a3;

import comp249.qq.a3.exceptions.csv.CSVDataMissingException;
import comp249.qq.a3.exceptions.csv.CSVFileInvalidException;

import java.io.*;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Utility class to group together all data and methods needed to convert a csv file to json format.
 *
 * <p>This class groups together the Scanner for input, the PrintWriter for output, the file name for
 * useful error messages, and all internal data needed for conversion.</p>
 *
 * <p>This class is responsible of opening a file for reading, opening a file for writer, parsing every
 * row of the csv file, converting the parsed data into json format, and writing that data to the output
 * file. When an error occurs, either it is treated by this class, or an exception can be thrown to the
 * calling method, depending on the severity of the error.</p>
 */
class CsvFileConverter implements Closeable {

    /**
     * Regular expression to match a csv field.
     *
     * <p>This pattern matches, when used in a record of a csv file, all single fields separated by a comma.
     * An item can be enclosed in quotation marks ("), and they must be if they contain a comma.
     * <strong>Examples of matching records:</strong><br/>
     * <tt>one,two,"three,four",,five,</tt><br/>
     * matches<br/>
     * <tt>(one), (two), (three,four), (), (five), ()</tt></p>
     *
     * <p><strong>Limitations:</strong><br/>
     * The regex does not match an item that contains a quotation mark if it is not enclosed in quotes.</p>
     *
     * <p>This regex was developed by myself and not copied from a random website.</p>
     */
    private static final String REGEX_CSV = "(?:(?<=^\"|,\").*?(?=\",|\"$))|(?:(?<=^|,)[^,\"]*?(?=,|$))";
    private static final Pattern CSV_PATTERN = Pattern.compile(REGEX_CSV);

    private final String fileName;
    private final Scanner inputFile;
    private String outFileName;
    private PrintWriter outputFile;
    private String[] header;
    private int lineCounter = 0;

    /**
     * Private constructor to enforce instantiation through the {@link CsvFileConverter#fromPath} method.
     *
     * @param fileName name of the file to be parsed
     * @param inputFile scanner reading a csv file
     */
    private CsvFileConverter(String fileName, Scanner inputFile) {
        this.fileName = fileName;
        this.inputFile = inputFile;
    }

    /**
     * Generates a CsvFileConverter object from the given csv file path.
     *
     * @param inputFilePath csv file to be converted
     * @return A newly created CsvFileConverter instance.
     * @throws FileNotFoundException if the given path doesn't point to an existing file
     */
    public static CsvFileConverter fromPath(Path inputFilePath) throws FileNotFoundException {
        String fileName = inputFilePath.getFileName().toString();

        Scanner inputFile;
        try {
            inputFile = new Scanner(new BufferedReader(new FileReader(inputFilePath.toFile())));
        } catch (FileNotFoundException e) {
            System.err.println("Could not open file '" + fileName + "' for reading.");
            System.err.println("Please check if file exists! Program will terminate after closing all open paths.");
            CSV2JSON.logPrintln("Could not open file '" + fileName + "' for reading.");
            CSV2JSON.logPrintln("Please check if file exists! Program will terminate after closing all open paths.");
            throw e;
        }

        return new CsvFileConverter(fileName, inputFile);
    }

    /**
     * Checks validity of this csv file.
     *
     * <p>Once the input file is opened for reading, attempts to parse the first row (the header)
     * of the input file to get the names of the fields.</p>
     *
     * @throws CSVFileInvalidException If the required csv format is not respected.<br/>
     *      This can happen on an empty header row, or on an empty field (ex: <tt>item1,,item2</tt>).
     * @throws IllegalStateException If the required calling order of the methods is not respected.
     */
    public void parseHeader() throws CSVFileInvalidException {
        if (this.inputFile == null) {
            throw new IllegalStateException("Attempt to parse header. Must first open a file.");
        }

        List<String> header = new ArrayList<>();

        // Used for meaningful log messages.
        List<Integer> emptyIdx = new ArrayList<>(); // List of the idx of empty fields
        boolean emptyField = false; // Turn 'true' to throw error

        String line = inputFile.nextLine();
        lineCounter++;

        // Exception on empty file. Assume first line is read.
        if (line == null) {
            CSV2JSON.logPrintln("File '" + fileName + "' is invalid.");
            CSV2JSON.logPrintln("\tThe file is empty.");
            throw new CSVFileInvalidException("File '" + fileName + "' is invalid: the file is empty.");
        }

        // Regex
        Matcher matcher = CSV_PATTERN.matcher(line);

        while (matcher.find()) {
            String field = matcher.group();

            // Empty field validation
            if (field.equals("")) {
                emptyField = true;
                emptyIdx.add(matcher.start() + 1);
            }

            header.add(field.trim());
        }

        // Exception on no matches
        if (header.isEmpty()) {
            CSV2JSON.logPrintln("File '" + fileName + "' is invalid.");
            CSV2JSON.logPrintln("\tEmpty header row: the first line is empty.");
            throw new CSVFileInvalidException("File '" + fileName + "' is invalid: has empty header row (first line).");
        }

        // Exception on empty field
        if (emptyField) {
            // Build pretty log message
            // Convert List<Integer> to int[]
            int[] emptyIdxArr = emptyIdx.stream().mapToInt(Integer::intValue).toArray();
            String msg = prettyLogError(emptyIdxArr, line.length());

            // Actual log and error messages
            CSV2JSON.logPrintln("File '" + fileName + "' is invalid.");
            CSV2JSON.logPrintln("\tEmpty field.");
            CSV2JSON.logPrintln("\t" + line); // Field1,,OtherField,
            CSV2JSON.logPrintln("\t" + msg);       //       ^^          ^
            throw new CSVFileInvalidException("File '" + fileName + "' is invalid: field is missing.");
        }
        this.header = header.toArray(new String[0]);
    }

    /**
     * Open the output files for writing.
     *
     * <p>Once the input file validity has been verified, attempts to open the corresponding output
     * file for writing.</p>
     *
     * @throws IOException If the output file cannot be created.
     * @throws IllegalStateException If the required calling order of the methods is not respected.
     */
    public void openOutputFile() throws IOException {
        if (this.header == null) {
            throw new IllegalStateException("Attempt to open an output file. Must first check validity of the input file.");
        }

        // Change extension for JSON
        this.outFileName = fileName.replaceAll("(?<=.\\.)\\w+$", "json");

        // Concat dir/file.json
        Path filePath = CSV2JSON.getOutputDirectory().resolve(outFileName);

        PrintWriter outputFile;
        try {
            outputFile =  new PrintWriter(new BufferedWriter(new FileWriter(filePath.toFile())));
        } catch (IOException e) {
            CSV2JSON.logPrintln("Could not open file '" + filePath + "' for writing.");
            throw new IOException("Could not open file '" + filePath + "' for writing.", e);
        }

        this.outputFile = outputFile;
    }

    /**
     * Convert the csv file into the output json file.
     *
     * <p>Once the output file is opened, parses the input file record by record, convert the row to json
     * and write it in the output file.</p>
     *
     * @throws IllegalStateException If the required calling order of the methods is not respected.
     */
    public void convertFile() {
        if (this.outputFile == null) {
            throw new IllegalStateException("Attempt to convert a file. Must fist open an output file.");
        }
        // Used to parse lines
        String[] record;

        // Start JSON array
        if (CSV2JSON.isCompactJSON()) {
            outputFile.print("[");
        } else {
            outputFile.println("[");
        }

        // Loop to convert data
        while (inputFile.hasNextLine()) {
            String line = inputFile.nextLine();
            lineCounter++;

            // Read and parse from input file
            try {
                record = readRecord(line);
            } catch (CSVDataMissingException e) {
                System.err.println(e.getMessage());
                continue; // Skip converting
            }

            // Convert to JSON and write to output file
            if (CSV2JSON.isCompactJSON()) {
                outputFile.print("{");
                int i = 0;
                while (i < record.length - 1) {
                    outputFile.printf("%s:%s,", header[i], record[i]);
                    i++;
                }
                outputFile.printf("%s:%s", header[i], record[i]);
                if (inputFile.hasNextLine()) {
                    outputFile.print("},");
                } else {
                    outputFile.print("}"); // Without comma
                }
            } else {
                outputFile.println("\t{");
                int i = 0;
                while (i < record.length - 1) {
                    outputFile.printf("\t\t\"%s\": %s,%n", header[i], stringify(record[i]));
                    i++;
                }
                outputFile.printf("\t\t\"%s\": %s%n", header[i], stringify(record[i]));
                if (inputFile.hasNextLine()) {
                    outputFile.println("\t},");
                } else {
                    outputFile.println("\t}"); // Without comma
                }
            }
        }

        // End JSON array
        if (CSV2JSON.isCompactJSON()) {
            outputFile.print("]");
        } else {
            outputFile.println("]");
        }

    }

    /**
     * Wraps JSON Strings in quotes and leave number as is.
     *
     * @param input the string to format for JSON
     * @return The input maybe surrounded by quotation marks.
     */
    private static String stringify(String input) {
        try {
            // If it can be converted to a Double, it is a number.
            Double.parseDouble(input);
            return input;
        } catch (NumberFormatException e) {
            return "\"" + input + "\"";
        }
    }

    /**
     * Extracts the individual fields from the given csv line.
     *
     * <p>Performs validation on the given string and throws an exception on invalid format.</p>
     *
     * @param line the csv line from which to extract the items
     * @return A string array containing all the items found is the given csv line.
     * @throws CSVDataMissingException If an empty field is found or if the number or fields is different
     *          than the header's.
     * @throws IllegalStateException If the required calling order of the methods is not respected.
     */
    private String[] readRecord(String line) throws CSVDataMissingException {
        List<String> record = new ArrayList<>();

        // Regex declarations
        Matcher matcher = CSV_PATTERN.matcher(line);

        // Used for meaningful log messages.
        List<Integer> emptyIdx = new ArrayList<>(); // List of the idx of empty fields
        boolean emptyField = false; // Turn 'true' to throw error


        // Validation and parsing
        String field;
        while (matcher.find()) {
            field = matcher.group();

            // Empty field validation
            if (field.equals("")) {
                emptyIdx.add(matcher.start());
                emptyField = true;
            }

            record.add(field.trim());
        }

        // Exception on empty field
        if (emptyField) {
            // Build pretty log message
            // Convert List<Integer> to int[]
            int[] emptyIdxArr = emptyIdx.stream().mapToInt(Integer::intValue).toArray();
            String msg = prettyLogError(emptyIdxArr, line.length());

            // Actual log and error messages
            CSV2JSON.logPrintln("In file '" + fileName + "' line " + lineCounter + ":");
            CSV2JSON.logPrintln("\tEmpty field.");
            CSV2JSON.logPrintln("\t" + line); // Field1,,OtherField,
            CSV2JSON.logPrintln("\t" + msg);  //       ^^          ^
            throw new CSVDataMissingException(
                    "In file '" + fileName + "' line " + lineCounter + " not converted to JSON: missing data.");
        }

        // Wrong number of fields
        int fieldNumDiff = record.size() - header.length;
        if (fieldNumDiff != 0) {
            CSV2JSON.logPrintln("In file '" + fileName + "' line " + lineCounter);
            CSV2JSON.logPrintln("\t" + line);
            CSV2JSON.logPrintln(String.format("%d too %s field%s.%n",
                    Math.abs(fieldNumDiff),
                    fieldNumDiff > 0 ? "many" : "few",
                    Math.abs(fieldNumDiff) != 1 ? "s" : ""));
            throw new CSVDataMissingException(
                    String.format("In file '%s' line %d not converted to JSON: %s data.%n",
                            fileName, lineCounter, fieldNumDiff > 0 ? "too much" : "missing"));
        }

        return record.toArray(new String[0]);
    }

    @Override public void close() {
        if (inputFile != null) {
            inputFile.close();
        }
        if (outputFile != null) {
            outputFile.flush();
            outputFile.close();
        }
        header = null;
    }

    /**
     * Returns the name of the output json file.
     * @return The name of the output json file.
     */
    public String getOutputFileName() {
        return this.outFileName;
    }

    /**
     * Returns whether the output file is created. Equivalent to the input file being valid.
     *
     * <p>This is to be used to filter the valid files out of a group of files.</p>
     *
     * @return Whether the output file is created.
     */
    public boolean isOutputCreated() {
        return this.outputFile != null;
    }

    /**
     * Formats a log error message for a record with an empty field.
     *
     * <p>When a csv record with at least one empty field is found, this method formats an
     * error message in the following way:<pre>
     *     one,two,,"four, five",
     *            ^^            ^
     * </pre></p>
     *
     * @param emptyIdx array containing the start index of all the empty fields,
     *                 as given by {@link Matcher#start()}.
     * @param lineLength length of the csv line
     * @return A String consisting of the formatted error message.
     */
    private static String prettyLogError(int[] emptyIdx, int lineLength) {
        StringBuilder msg = new StringBuilder();

        for (int idx : emptyIdx) {
            if (idx == 0) {
                // ^,item
                msg.append("^");
            } else if (idx == lineLength) {
                // item,$
                int len = idx - msg.length() - 1;
                msg.append(" ".repeat(len));
                msg.append("^");
            } else {
                // item,,item
                int len = idx - msg.length() - 1;
                msg.append(" ".repeat(len));
                msg.append("^^");
            }
        }

        return msg.toString();
    }
}
