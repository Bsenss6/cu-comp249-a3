/*
 * csv2json: a program to convert CSV files into JSON files.
 * Author: Dannick BUJOLD-SENSS.
 * Student ID: 40180290.
 * Course: COMP 249, Section QQ.
 * With: Dr. Kaustubha Mendhurwar.
 * Project: Assignment 3.
 */

package comp249.qq.a3.cla;

import comp249.qq.a3.exceptions.cla.CLAException;
import comp249.qq.a3.exceptions.cla.InvalidArgumentException;
import comp249.qq.a3.exceptions.cla.MissingArgumentException;
import comp249.qq.a3.exceptions.cla.UnrecognizedOptionException;

import java.nio.file.InvalidPathException;
import java.nio.file.Paths;

/**
 * Parser for command line arguments.
 *
 * This class is used to obtain a usable {@link Options} object from a list of arguments passed to the program at
 * invocation.
 *
 * <p>This parser supports the long options to be separated from a mandatory argument by a space or by an equal sign.
 * <br/>Both examples supported:
 * <br/>{@code program --output output.txt}
 * <br/>{@code program --output=output.txt}</p>
 *
 * <p>This parser supports the short options to be packed with their argument.
 * <br/>Both examples supported:
 * <br/>{@code program -o output.txt}
 * <br/>{@code program -ooutput.txt}</p>
 *
 * <p>This parser supports the short options to be chained together.
 * <br/>Both examples supported:
 * <br/>{@code program -a -b -c}
 * <br/>{@code program -abc}</p>
 */
public class ArgsParser {

    private final OptionsBean bean = new OptionsBean();
    private final String[] args;
    private ParserState state = ParserState.NORMAL;

    /**
     * Constructs a parser for the given command line arguments.
     * @param args command line arguments to be parsed
     */
    public ArgsParser(String[] args) {
        this.args = args;
    }

    /**
     * Parses the given arguments into a usable {@link Options} object.
     *
     * @return An {@link Options} object encoding the passed arguments.
     * @throws CLAException if a long or short option is not recognized or if a mandatory argument is missing.
     */
    public Options parse() throws CLAException {

        for (String arg : this.args) {
            if (arg.startsWith("--")) {
                parseLongOpt(arg);
            } else if (arg.startsWith("-")) {
                parseShortOpt(arg);
            } else {
                parseNonOpt(arg);
            }
        }

        // FILE is a mandatory argument
        if (bean.getFiles().length == 0) {
            throw new MissingArgumentException("Missing mandatory argument FILE.");
        }

        return new Options(this.bean);
    }

    /**
     * Parses an option in long form.
     *
     * <p>This parser supports the long options to be separated from a mandatory argument by a space or by an equal sign.
     * <br/>Both formats are supported:
     * <br/>{@code program --output output.txt}
     * <br/>{@code program --output=output.txt}</p>
     *
     * @param arg String argument from which to extract information
     * @throws CLAException when the parser is expecting an argument, because of its state, but receives an option,
     *                       or when an expected format is not respected.
     */
    private void parseLongOpt(String arg) throws CLAException {
        checkExpectedArg();

        // Support for option and argument separated by '='
        //   program --out output.txt
        //   program --out=output.txt
        int idx = arg.indexOf('=');
        String arg2 = "";
        if (idx != -1) {
            arg2 = arg.substring(idx + 1); // after '='
            arg = arg.substring(0, idx);   // before '='
        }

        switch (arg) {
            case "--help" -> bean.setHelpLong();
            case "--quiet" -> bean.setQuiet();
            case "--about" -> bean.setAbout();
            case "--interactive" -> bean.setInteractive();
            case "--compact-json" -> bean.setCompactJSON();
            case "--directory" -> {
                if (arg2.equals("")) {
                    this.state = ParserState.ARG_DIR_LONG;
                } else try {
                    bean.setOutputDirectory(Paths.get(arg2));
                } catch (InvalidPathException e) {
                    throw new InvalidArgumentException(e.getInput(), arg, e.getReason());
                }
            }
            case "--log" -> {
                if (arg2.equals("")) {
                    this.state = ParserState.ARG_LOG_LONG;
                } else try {
                    bean.setLogFile(Paths.get(arg2));
                } catch (InvalidPathException e) {
                    throw new InvalidArgumentException(e.getInput(), arg, e.getReason());
                }
            }
            default -> throw new UnrecognizedOptionException("--" + arg);
        }
    }

    /**
     * Parses options in short form (flags).
     *
     * <p>This parser supports the short options to be packed with their argument.
     * <br/>Both formats are supported:
     * <br/>{@code program -o output.txt}
     * <br/>{@code program -ooutput.txt}</p>
     *
     * <p>This parser supports the short options to be chained together.
     * <br/>Both formats are supported:
     * <br/>{@code program -a -b -c}
     * <br/>{@code program -abc}</p>
     *
     * @param arg String argument from which to extract information
     * @throws CLAException when the parser is expecting an argument, because of its state, but receives an option,
     *                       or when an expected format is not respected.
     */
    private void parseShortOpt(String arg) throws CLAException {
        checkExpectedArg();

        // parses grouped short option (ex: program -abc)
        char[] flags = arg.toCharArray();
        for (int i = 1; i < flags.length; i++) {

            switch (flags[i]) {

                case 'h': // short help
                    bean.setHelpShort();
                    break;

                case 'a': // about
                    bean.setAbout();
                    break;

                case 'i': // interactive
                    bean.setInteractive();
                    break;

                case 'q': // quiet
                    bean.setQuiet();
                    break;

                case 'c': // compact JSON
                    bean.setCompactJSON();
                    break;

                case 'd': // output directory
                    // IF is last flag, expect DIR on next arg
                    // (ex: program -o output.txt)
                    if (i == flags.length - 1) {
                        this.state = ParserState.ARG_DIR_SHORT;
                    }
                    // ELSE expect DIR on all next chars
                    // (ex: program -ooutput.txt)
                    else {
                        String dirPath = arg.substring(i + 1);
                        try {
                            bean.setOutputDirectory(Paths.get(dirPath));
                        } catch (InvalidPathException e) {
                            throw new InvalidArgumentException(e.getInput(), "-" + flags[i], e.getReason());
                        }
                    }
                    break;

                case 'l': // log file
                    // IF is last flag, expect LOG on next arg
                    // (ex: program -o output.txt)
                    if (i == flags.length - 1) {
                        this.state = ParserState.ARG_LOG_SHORT;
                    }
                    // ELSE expect LOG on all next chars
                    // (ex: program -ooutput.txt)
                    else {
                        String logFile = arg.substring(i + 1);
                        try {
                            bean.setOutputDirectory(Paths.get(logFile));
                        } catch (InvalidPathException e) {
                            throw new InvalidArgumentException(e.getInput(), "-" + flags[i], e.getReason());
                        }
                    }
                    break;

                default:
                    throw new UnrecognizedOptionException("-" + flags[i]);
            }
        }
    }

    /**
     * Parses a non-option argument.
     *
     * <p>This can be a DIR for the '-d' or '--directory' options, or one of the FILE...</p>
     *
     * @param arg String argument for which to extract information
     * @throws CLAException when the argument does not respect the expected format.
     */
    private void parseNonOpt(String arg) throws CLAException {

        switch (this.state) {
            case NORMAL -> {
                try {
                    bean.addFile(Paths.get(arg));
                } catch (InvalidPathException e){
                    throw new InvalidArgumentException("Invalid argument '" + e.getInput() + "' for FILE.");
                }
            }
            case ARG_DIR_LONG -> {
                try {
                    bean.setOutputDirectory(Paths.get(arg));
                    this.state = ParserState.NORMAL;
                } catch (InvalidPathException e){
                    throw new InvalidArgumentException(e.getInput(), "--directory", e.getReason());
                }
            }
            case ARG_DIR_SHORT -> {
                try {
                    bean.setOutputDirectory(Paths.get(arg));
                    this.state = ParserState.NORMAL;
                } catch (InvalidPathException e){
                    throw new InvalidArgumentException(e.getInput(), "-d", e.getReason());
                }
            }
            case ARG_LOG_LONG -> {
                try {
                    bean.setLogFile(Paths.get(arg));
                    this.state = ParserState.NORMAL;
                } catch (InvalidPathException e){
                    throw new InvalidArgumentException(e.getInput(), "--log", e.getReason());
                }
            }
            case ARG_LOG_SHORT -> {
                try {
                    bean.setLogFile(Paths.get(arg));
                    this.state = ParserState.NORMAL;
                } catch (InvalidPathException e){
                    throw new InvalidArgumentException(e.getInput(), "-l", e.getReason());
                }
            }
        }
    }

    /**
     * Checks if the parser expected an arg when receiving an option.
     *
     * <p>If the parser is in a state where it expects an argument but instead receives an option,
     * this method will throw an Exception for missing argument.Does nothing otherwise.</p>
     *
     * @throws MissingArgumentException when expecting an argument but receiving an option.
     */
    private void checkExpectedArg() throws MissingArgumentException {
        switch (this.state) {
            case NORMAL -> {}
            case ARG_DIR_SHORT -> throw new MissingArgumentException("Option '-d' requires an argument.");
            case ARG_DIR_LONG -> throw new MissingArgumentException("Option '--directory' requires an argument.");
            case ARG_LOG_SHORT -> throw new MissingArgumentException("Option '-l' requires an argument.");
            case ARG_LOG_LONG -> throw new MissingArgumentException("Option '--log' requires an argument.");
        }
    }
}
