/*
 * csv2json: a program to convert CSV files into JSON files.
 * Author: Dannick BUJOLD-SENSS.
 * Student ID: 40180290.
 * Course: COMP 249, Section QQ.
 * With: Dr. Kaustubha Mendhurwar.
 * Project: Assignment 3.
 */

package comp249.qq.a3.cla;

import java.nio.file.Path;
import java.util.Arrays;

public class Options {

    private final Path[] files;
    private final Path outputDirectory;
    private final Path logFile;
    private final boolean compactJSON;
    private final boolean interactive;
    private final boolean quiet;
    private final boolean helpShort;
    private final boolean helpLong;
    private final boolean about;

    /**
     * Package private bean constructor
     *
     * <p>This makes the only possible instantiation through the bean constructor. The {@link Options} object is
     * created by copying all the corresponding fields of the given {@link OptionsBean}.
     * @param bean Bean object from which to build this
     */
    Options(OptionsBean bean) {
        this.files = bean.getFiles();
        this.outputDirectory = bean.getOutputDirectory();
        this.logFile = bean.getLogFile();
        this.compactJSON = bean.isCompactJSON();
        this.interactive = bean.isInteractive();
        this.quiet = bean.isQuiet();
        this.helpShort = bean.isHelpShort();
        this.helpLong = bean.isHelpLong();
        this.about = bean.isAbout();
    }

    /**
     * Returns the name of the CSV files to be parsed.
     *
     * <p>Actually returns a copy of the owned array to ensure immutability of this object.</p>
     *
     * @return The CSV files to be parsed as an array of Strings.
     */
    public Path[] getFiles() {
        return Arrays.copyOf(this.files, this.files.length);
    }

    /**
     * Returns the destination folder for the output JSON files.
     *
     * <p>This defaults to ''.</p>
     *
     * @return The destination folder for where to write the output JSON files.
     */
    public Path getOutputDirectory() {
        return outputDirectory;
    }

    /**
     * Returns the name of the file where to put log messages.
     *
     * <p>This defaults to 'log.txt'.</p>
     *
     * @return The name of the log file.
     */
    public Path getLogFile() {
        return logFile;
    }

    /**
     * Returns if the output JSON files are in a compact form.
     *
     * <p>In the compact form, no unnecessary space is printed and there are no line breaks.
     * This is useful for sending data over a stream.</p>
     *
     * @return <code>true</code> if the JSON files should be compact, <code>false</code> if not.
     */
    public boolean isCompactJSON() {
        return compactJSON;
    }

    /**
     * Specify if the program should run in interactive mode.
     *
     * <p>In this mode, after parsing the CSV and writing the JSON, the user is prompted for an
     * output JSON file to be displayed to stdout.</p>
     *
     * <p>If this flag is set, the quiet mode flag is false.</p>
     *
     * @return Whether the program is in interactive mode.
     */
    public boolean isInteractive() {
        return interactive;
    }

    /**
     * Specify if the program should run in quiet mode.
     *
     * <p>If set, there nothing is printed to the standard output.</p>
     *
     * <p>If the program is launched in interactive mode (using the {@code -i} or {@code --interactive} flag),
     * this argument is set to false. In interactive mode, standard output is needed.</p>
     *
     * @return Whether the quiet flag is set.
     */
    public boolean isQuiet() {
        return quiet;
    }

    /**
     * Specify if the program should display the short help message.
     *
     * <p>If set, the short help message is displayed and the program exits.</p>
     *
     * <p>This argument takes precedence over all others except the long help.
     * If the {@code -h} flag is passed in the arguments, and {@code --help} is not, then {@code true}
     * is returned.</p>
     *
     * @return Whether the program should display the short help message.
     */
    public boolean isHelpShort() {
        return helpShort;
    }

    /**
     * Specify if the program should display the long help message.
     *
     * <p>If set, the long help message is displayed and the program exits.</p>
     *
     * <p>This argument takes precedence over all others, meaning that {@code true} is returned if the
     * {@code --help} flag is passed in the arguments.</p>
     *
     * @return Whether the program should display the long help message.
     */
    public boolean isHelpLong() {
        return helpLong;
    }

    /**
     * Specify if the program should display the about message.
     *
     * <p>If set, the about message is displayed and the program exists.</p>
     *
     * <p>This argument takes precedence over all others except the help flags.</p>
     *
     * @return Whether the program should display the long help message.
     */
    public boolean isAbout() {
        return about;
    }
}
