/*
 * csv2json: a program to convert CSV files into JSON files.
 * Author: Dannick BUJOLD-SENSS.
 * Student ID: 40180290.
 * Course: COMP 249, Section QQ.
 * With: Dr. Kaustubha Mendhurwar.
 * Project: Assignment 3.
 */

package comp249.qq.a3.cla;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * Bean class to construct an {@link Options} object incrementally.
 *
 * <p>Apply normalization on place.<br/>
 * There is no need to verify flags compatibility or invalid values passed,
 * as those checks are made and corrected when calling the setXXX methods of this class.<rb/>
 * The validation rules applied by this object are:
 * <ul>
 *     <li>Avoids duplicate names in FILE ...</li>
 *     <li>Sets 'quiet' flag to {@code false} if 'interactive' is set (as 'interactive' needs stdout).</li>
 * </ul>
 * </p>
 */
class OptionsBean {

    private final List<Path> files = new ArrayList<>();
    private Path outputDirectory = Paths.get("");
    private Path logFile = Paths.get("log.txt");
    private boolean compactJSON;
    private boolean interactive;
    private boolean quiet;
    private boolean helpShort;
    private boolean helpLong;
    private boolean about;

    Path[] getFiles() {
        return this.files.toArray(new Path[0]);
    }

    void addFile(Path file) {
        if (file == null) {
            throw new NullPointerException("Parameter 'file' is null.");
        }
        // Avoid duplicate file names
        if (!this.files.contains(file)) {
            this.files.add(file);
        }
    }

    Path getOutputDirectory() {
        return outputDirectory;
    }

    void setOutputDirectory(Path outputDirectory) {
        if (outputDirectory == null) {
            throw new NullPointerException("Parameter 'outputDirectory' is null.");
        }
        this.outputDirectory = outputDirectory;
    }

    public Path getLogFile() {
        return logFile;
    }

    void setLogFile(Path logFile) {
        if (logFile == null) {
            throw new NullPointerException("Parameter 'logFile' is null.");
        }
        this.logFile = logFile;
    }

    public boolean isCompactJSON() {
        return compactJSON;
    }

    public void setCompactJSON() {
        this.compactJSON = true;
    }

    boolean isInteractive() {
        return interactive;
    }

    void setInteractive() {
        this.interactive = true;
        this.quiet = false;

        // 'interactive' needs output to stdout, thus overwrite the 'quiet' flag.
    }

    boolean isQuiet() {
        return quiet;
    }

    void setQuiet() {
        // 'quiet' is set iff 'interactive' is not.
        this.quiet = !interactive;

        // 'interactive' needs output to stdout, thus overwrite the 'quiet' flag.
    }

    boolean isHelpShort() {
        return helpShort;
    }

    void setHelpShort() {
        this.helpShort = true;
    }

    boolean isHelpLong() {
        return helpLong;
    }

    void setHelpLong() {
        this.helpLong = true;
    }

    public boolean isAbout() {
        return about;
    }

    public void setAbout() {
        this.about = true;
    }
}
