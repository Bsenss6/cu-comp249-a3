/*
 * csv2json: a program to convert CSV files into JSON files.
 * Author: Dannick BUJOLD-SENSS.
 * Student ID: 40180290.
 * Course: COMP 249, Section QQ.
 * With: Dr. Kaustubha Mendhurwar.
 * Project: Assignment 3.
 */

package comp249.qq.a3.cla;

/**
 * Describes in what state is the parser.
 *
 * <p>It can accept new arguments ('NORMAL') or be waiting for an argument of a previous option.</p>
 */
public enum ParserState {
    NORMAL,
    ARG_DIR_SHORT,
    ARG_DIR_LONG,
    ARG_LOG_SHORT,
    ARG_LOG_LONG
}
