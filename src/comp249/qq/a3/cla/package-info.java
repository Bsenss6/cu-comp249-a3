/*
 * csv2json: a program to convert CSV files into JSON files.
 * Author: Dannick BUJOLD-SENSS.
 * Student ID: 40180290.
 * Course: COMP 249, Section QQ.
 * With: Dr. Kaustubha Mendhurwar.
 * Project: Assignment 3.
 */

/**
 * Provides the classes necessary to use the command line arguments passed to the program at invocation.
 *
 * <p>The public API allow access to the {@link comp249.qq.a3.cla.Options} class to encapsulate, and use
 * easily during the program, the various options passed through the CLAs. There is also available the
 * {@link comp249.qq.a3.cla.ArgsParser}</p> used to extract information from the arguments and to construct an Options
 * object.</p>
 *
 * <p>The required use of this package's classes is the following:
 * <pre><code>
 * ArgsParser parser = new ArgsParser(args);
 * Options options = parser.parse();
 * </code></pre></p>
 *
 * <p>
 * Alternatively, one can extract the options in a more compact manner:
 * <pre><code>
 * Options options = new ArgsParser(args).parse();
 * </code></pre>
 * </p>
 */
package comp249.qq.a3.cla;