/*
 * csv2json: a program to convert CSV files into JSON files.
 * Author: Dannick BUJOLD-SENSS.
 * Student ID: 40180290.
 * Course: COMP 249, Section QQ.
 * With: Dr. Kaustubha Mendhurwar.
 * Project: Assignment 3.
 */

package comp249.qq.a3.exceptions.cla;

/**
 * Custom exception to be thrown when parsing the arguments fails.
 *
 * <p>This exception can be thrown for several reasons, such as a mandatory argument omitted,
 * an unrecognized option or when incompatible options are set.</p>
 */
public class CLAException extends Exception {

    /**
     * Default constructor with an empty message.
     */
    public CLAException() {
    }

    /**
     * Generates an CLAException with the specified message.
     *
     * @param message detail message to be displayed.
     */
    public CLAException(String message) {
        super(message);
    }
}
