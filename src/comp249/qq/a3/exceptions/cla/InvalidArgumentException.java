/*
 * csv2json: a program to convert CSV files into JSON files.
 * Author: Dannick BUJOLD-SENSS.
 * Student ID: 40180290.
 * Course: COMP 249, Section QQ.
 * With: Dr. Kaustubha Mendhurwar.
 * Project: Assignment 3.
 */

package comp249.qq.a3.exceptions.cla;

/**
 * Custom exception to be thrown when a given argument does not respect the required format.
 */
public class InvalidArgumentException extends CLAException {

    /**
     * Default constructor with an empty message.
     */
    public InvalidArgumentException() {
    }

    /**
     * Generates an exception with the specified message.
     *
     * @param message detailed message to be displayed.
     */
    public InvalidArgumentException(String message) {
        super(message);
    }

    /**
     * Generates a default exception message from the specified option and its argument names.
     *
     * @param argument invalid argument for the option
     * @param option name of the option
     */
    public InvalidArgumentException(String argument, String option) {
        super("Invalid argument '" + argument + "' for option '" + option + "'.");
    }

    /**
     * Generates a default exception message from the specified option name, its arguments, and a detailed message.
     *
     * @param argument invalid argument for the option
     * @param option name of the option
     * @param details detail message
     */
    public InvalidArgumentException(String argument, String option, String details) {
        super("Invalid argument '" + argument + "' for option '" + option + "'.\n" + details);
    }
}
