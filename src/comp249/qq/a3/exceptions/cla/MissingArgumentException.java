/*
 * csv2json: a program to convert CSV files into JSON files.
 * Author: Dannick BUJOLD-SENSS.
 * Student ID: 40180290.
 * Course: COMP 249, Section QQ.
 * With: Dr. Kaustubha Mendhurwar.
 * Project: Assignment 3.
 */

package comp249.qq.a3.exceptions.cla;

/**
 * Custom exception to be thrown when a mandatory argument is missing.
 */
public class MissingArgumentException extends CLAException {

    /**
     * Default constructor with an empty message.
     */
    public MissingArgumentException() {
    }

    /**
     * Generates an exception with the specified message.
     *
     * @param message detailed message to be displayed.
     */
    public MissingArgumentException(String message) {
        super(message);
    }
}
