/*
 * csv2json: a program to convert CSV files into JSON files.
 * Author: Dannick BUJOLD-SENSS.
 * Student ID: 40180290.
 * Course: COMP 249, Section QQ.
 * With: Dr. Kaustubha Mendhurwar.
 * Project: Assignment 3.
 */

package comp249.qq.a3.exceptions.cla;

/**
 * Custom exception to be thrown when a given option is not recognized by the parser.
 */
public class UnrecognizedOptionException extends CLAException {

    /**
     * Default constructor with an empty message.
     */
    public UnrecognizedOptionException() {
    }

    /**
     * Generates a default exception message from the specified option name.
     *
     * @param option detailed option to be displayed.
     */
    public UnrecognizedOptionException(String option) {
        super("Unrecognized option '" + option + "'.");
    }

    /**
     * Generates a custom exception with the specified message.
     *
     * @param message message of the exception
     * @param custom flag to call the custom constructor
     */
    public UnrecognizedOptionException(String message, boolean custom) {
        super(message);
    }

}
