/*
 * csv2json: a program to convert CSV files into JSON files.
 * Author: Dannick BUJOLD-SENSS.
 * Student ID: 40180290.
 * Course: COMP 249, Section QQ.
 * With: Dr. Kaustubha Mendhurwar.
 * Project: Assignment 3.
 */

/**
 * Exceptions package for command line arguments parsing exceptions.
 *
 * <p>This package contains Exceptions to accurately inform the user of the invalid cla given to the program.</p>
 */
package comp249.qq.a3.exceptions.cla;