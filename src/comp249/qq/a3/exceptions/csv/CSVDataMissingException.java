/*
 * csv2json: a program to convert CSV files into JSON files.
 * Author: Dannick BUJOLD-SENSS.
 * Student ID: 40180290.
 * Course: COMP 249, Section QQ.
 * With: Dr. Kaustubha Mendhurwar.
 * Project: Assignment 3.
 */

package comp249.qq.a3.exceptions.csv;

/**
 * Custom exception for invalid CSV record.
 *
 * <p>This exception is thrown when a record in a file has invalid CSV format.
 * This can be caused by an empty field.</p>
 */
public class CSVDataMissingException extends CSVInvalidException {

    /**
     * Default constructor for a CSVDataMissingException.
     *
     * <p>Generates a CSVDataMissingException object with the default error message of the parent class.</p>
     */
    public CSVDataMissingException() {
        super("Error: missing data in record.");
    }

    /**
     * Constructs a new CSVDataMissingException with the specified detail message.
     *
     * <p>Generates a CSVDataMissingException object with the specified error message.</p>
     *
     * @param message detailed message
     */
    public CSVDataMissingException(String message) {
        super(message);
    }
}
