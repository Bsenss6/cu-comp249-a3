/*
 * csv2json: a program to convert CSV files into JSON files.
 * Author: Dannick BUJOLD-SENSS.
 * Student ID: 40180290.
 * Course: COMP 249, Section QQ.
 * With: Dr. Kaustubha Mendhurwar.
 * Project: Assignment 3.
 */

package comp249.qq.a3.exceptions.csv;

/**
 * Custom exception for invalid csv file.
 *
 * <p>This exception is thrown when a file has invalid CSV format. This can be caused by an empty header field.</p>
 */
public class CSVFileInvalidException extends CSVInvalidException {

    /**
     * Default constructor for a CSVFileInvalidException.
     *
     * <p>Generates a CSVFileInvalidException object with the default error message of the parent class.</p>
     */
    public CSVFileInvalidException() {
        super("Error: Invalid file.");
    }

    /**
     * Parametrized constructor for a CSVFileInvalidException.
     *
     * <p>Generates a CSVFileInvalidException object with the specified error message.</p>
     *
     * @param message detailed message
     */
    public CSVFileInvalidException(String message) {
        super(message);
    }
}
