/*
 * csv2json: a program to convert CSV files into JSON files.
 * Author: Dannick BUJOLD-SENSS.
 * Student ID: 40180290.
 * Course: COMP 249, Section QQ.
 * With: Dr. Kaustubha Mendhurwar.
 * Project: Assignment 3.
 */

package comp249.qq.a3.exceptions.csv;

/**
 * Parent custom exception class for the CSV2JSON application.
 * 
 * <p>This class is used to handle different exceptions that can occur during
 * the execution of the program.</p>
 */
public class CSVInvalidException extends Exception {

    /**
     * Default constructor for an CSVInvalidException.
     * 
     * <p>Generates an CSVInvalidException object with the default error message:
     * &quot;Error: Input row cannot be parsed due to missing information&quot;.</p>
     */
    public CSVInvalidException() {
        super("Error: Input row cannot be parsed due to missing information");
    }

    /**
     * Parametrized constructor for an CSVInvalidException.
     * 
     * <p>Generates an CSVInvalidException object with the specified error message.</p>
     *
     * @param message detailed message
     */
    public CSVInvalidException(String message) {
        super(message);
    }
    
}
