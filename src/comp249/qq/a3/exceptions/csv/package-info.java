/*
 * csv2json: a program to convert CSV files into JSON files.
 * Author: Dannick BUJOLD-SENSS.
 * Student ID: 40180290.
 * Course: COMP 249, Section QQ.
 * With: Dr. Kaustubha Mendhurwar.
 * Project: Assignment 3.
 */

/**
 * Exceptions package for CSV parsing related exceptions.
 *
 * <p>Contains Exceptions that can be thrown when different CSV format errors are found.</p>
 */
package comp249.qq.a3.exceptions.csv;