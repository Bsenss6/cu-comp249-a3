/*
 * csv2json: a program to convert CSV files into JSON files.
 * Author: Dannick BUJOLD-SENSS.
 * Student ID: 40180290.
 * Course: COMP 249, Section QQ.
 * With: Dr. Kaustubha Mendhurwar.
 * Project: Assignment 3.
 */

/**
 * Exceptions package containing all custom exceptions for the program.
 * 
 * <p> This package provides the application with a set of custom exceptions to
 * handle different problems that might appear.</p>
 */
package comp249.qq.a3.exceptions;
